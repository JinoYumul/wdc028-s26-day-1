import React, { useContext } from 'react';
import Course from '../components/Course';
import { Table, Button } from 'react-bootstrap'

import UserContext from '../UserContext'
import coursesData from '../data/courses';

export default function Courses() {

	//consume UserContext and destructure to access the user state defined in App
	const { user } = useContext(UserContext)

	//create multiple course components with the content of coursesData
	const courses = coursesData.map(courseData => {
		if(courseData.onOffer === true){
			return (
				<Course key={courseData.id} course={courseData}/>
			)
		}else{
			return null;
		}
	})

	const coursesRows = coursesData.map(courseData => {
		return(
			<tr key={courseData.id}>
				<td>{courseData.id}</td>
				<td>{courseData.name}</td>
				<td>&#8369; {courseData.price}</td>
				<td>{courseData.onOffer ? 'open': 'closed'}</td>
				<td>{courseData.start_date}</td>
				<td>{courseData.end_date}</td>
				<td>
					<Button variant="warning">Update</Button>
					<Button variant="danger">Disable</Button>
				</td>
			</tr>
		)
	})

	return (
		user.isAdmin === true
		?	<React.Fragment>
				<h1>Course Dashboard</h1>
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Price</th>
							<th>Status</th>
							<th>Start Date</th>
							<th>End Date</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						{coursesRows}
					</tbody>
				</Table>
			</React.Fragment>
			:
			<React.Fragment>
				{courses}
			</React.Fragment>
	)
}