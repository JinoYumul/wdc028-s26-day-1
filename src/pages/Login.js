import React, { useState, useContext, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';
import users from '../data/users';

export default function Login(){

	//consume the UserContext object via useContext()
	const { user, setUser } = useContext(UserContext)

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [willRedirect, setWillRedirect] = useState('');

	useEffect(() => {
		console.log(`User with email ${user.email} is logged in.`)
	}, [user.isAdmin, user.email])

	function authenticate(e){
		e.preventDefault();

		const match = users.find(user => {
			return(user.email === email && user.password === password)
		})

		if(match){
			//set the email of the authenticated user in the local storage
			localStorage.setItem('email', email)
			localStorage.setItem('isAdmin', match.isAdmin)

			//set the global state to have properties obtained from local storage
			setUser({
				email: localStorage.getItem('email'),
				isAdmin: match.isAdmin
			})

			setEmail('');
			setPassword('');

			alert('You are now logged in.')

			setWillRedirect(true);
		}else{
			alert('Authentication failed.')
		}
	}	

	return(
		willRedirect === true
		? <Redirect to='/courses'/>

	    : <Form onSubmit={(e) => authenticate(e)}>
		    <Form.Group controlId="userEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
		    </Form.Group>

		    <Form.Group controlId="password">
		        <Form.Label>Password</Form.Label>
		        <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} required/>
		    </Form.Group>

		    <Button variant="primary" type="submit">Submit</Button>
        </Form>
	)
}